// @APIVersion 1.0.0
// @Title beego Test API
// @Description beego has a very cool tools to autogenerate documents for your API
// @Contact astaxie@gmail.com
// @TermsOfServiceUrl http://beego.me/
// @License Apache 2.0
// @LicenseUrl http://www.apache.org/licenses/LICENSE-2.0.html
package routers

import (
	"hello/controllers"

	"github.com/astaxie/beego"
)


func init() {
	
	// init namespace
	ns := beego.NewNamespace("/api/v1",

		beego.NSNamespace("/person",
			// get all persons
			beego.NSRouter("/", &controllers.UserController{}, "get:GetAllPerson"),

			// add new person
			//beego.NSRouter("/", &controllers.UserController{}, "post:AddNewPerson"),

			// add new blog
			beego.NSRouter("/", &controllers.BlogController{}, "post:AddNewBlog"),

			// update an existing person
			beego.NSRouter("/:id", &controllers.UserController{}, "put:UpdatePerson"),

			// delete a person
			beego.NSRouter("/:id", &controllers.UserController{}, "delete:DeletePerson"),

			// get a person with id
			beego.NSRouter("/:id", &controllers.UserController{}, "get:GetPersonById"),
		),
	)

	// register namespace
	beego.AddNamespace(ns)
}



/*
func init() {
	ns := beego.NewNamespace("/v1",
		beego.NSNamespace("/object",
			beego.NSInclude(
				&controllers.ObjectController{},
			),
		),
		beego.NSNamespace("/user",
			beego.NSInclude(
				&controllers.UserController{},
			),
		),
	)
	beego.AddNamespace(ns)
}
*/