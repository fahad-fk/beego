package controllers

import(
	"encoding/json"
	models "hello/models"
	"github.com/astaxie/beego"
)

type BlogController struct {
	beego.Controller
}



func (uc *BlogController) AddNewBlog() {
	var u models.Blog
	json.Unmarshal(uc.Ctx.Input.RequestBody, &u)
	blog := models.InsertOneBlog(u)
	uc.Data["json"] = blog
	uc.ServeJSON()
}