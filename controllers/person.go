package controllers

import(
	"encoding/json"
	"strconv"
	models "hello/models"
	"github.com/astaxie/beego"
)

type UserController struct {
	beego.Controller
}

func (uc *UserController) GetAllPerson() {
	uc.Data["json"] = models.GetAllPerson()
	uc.ServeJSON()
}

func (uc *UserController) DeletePerson() {
	// get id from query string and convert it to int
	id, _ := strconv.Atoi(uc.Ctx.Input.Param(":id"))

	// delete user
	deleted := models.DeletePerson(id)

	// generate response
	uc.Data["json"] = map[string]bool{"deleted": deleted}
	uc.ServeJSON()
}

func (uc *UserController) AddNewPerson() {
	var u models.Person
	json.Unmarshal(uc.Ctx.Input.RequestBody, &u)
	person := models.InsertOnePerson(u)
	uc.Data["json"] = person
	uc.ServeJSON()
}

func (uc *UserController) GetPersonById() {
	// get the id from query string
	id, _ := strconv.Atoi(uc.Ctx.Input.Param(":id"))

	// get user
	user := models.GetPersonById(id)

	// generate response
	uc.Data["json"] = user
	uc.ServeJSON()
}

//update

func (uc *UserController) UpdatePerson() {
	id, _ := strconv.Atoi(uc.Ctx.Input.Param(":id"))
	var u models.Person
	json.Unmarshal(uc.Ctx.Input.RequestBody, &u)
	person := models.UpdatePerson(id,u)
	uc.Data["json"] = person
	uc.ServeJSON()
}
