package main

import (
	_ "hello/routers"
	//models "hello/models"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "github.com/lib/pq"
    "github.com/astaxie/beego"
    "github.com/astaxie/beego/orm"
    //"fmt"
)

func init(){
    orm.RegisterDriver("postgres", orm.DRPostgres)
    orm.RegisterDataBase("default", 
        "postgres",
        "user=fadi password=test123 dbname=gorm sslmode=disable");

    orm.RunSyncdb("default", false, true)
}

func main() {
    orm.Debug = true

    o := orm.NewOrm()
    o.Using("default")
    //p := new(models.Person)
    //p.FirstName = "Fahad"
    //p.LastName = "Khan"
	//id, err := o.Insert(p)
    //fmt.Printf("ID: %d, ERR: %v\n", id, err)
    beego.Run()
}



/*func main() {
	if beego.BConfig.RunMode == "dev" {
		beego.BConfig.WebConfig.DirectoryIndex = true
		beego.BConfig.WebConfig.StaticDir["/swagger"] = "swagger"
	}
	beego.Run()
}*/
