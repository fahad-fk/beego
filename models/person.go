package models

import(
	"github.com/astaxie/beego/orm"
)

type Person struct {
	//Parsing JSON into the struct
	ID        int   `json:"id"`
	FirstName string `json:"firstname"`
	LastName  string `json:"lastname"`
}


func init() {
    // Need to register model in init
    orm.RegisterModel(new(Person))
}

func GetAllPerson() []*Person {
	o := orm.NewOrm()
	var persons []*Person
	o.QueryTable(new(Person)).All(&persons)

	return persons
}


/*
func InsertOnePerson(person Person) *Person {
	o := orm.NewOrm()
	qs := o.QueryTable(new(Person))

	// get prepared statement
	i, _ := qs.PrepareInsert()

	var u Person

	// Insert
	id, err := i.Insert(&person)
	if err == nil {
		// successfully inserted
		u = Person{ID: int(id)}
		err := o.Read(&u)
		if err == orm.ErrNoRows {
			return nil
		}
	} else {
		return nil
	}
	return &u
}
*/



func DeletePerson(id int) bool {
	o := orm.NewOrm()
	_, err := o.Delete(&Person{ID: id})
	if err == nil {
		// successfull
		return true
	}

	return false
}

func InsertOnePerson(person Person) *Person {
	o := orm.NewOrm()
	var u Person
	_, err := o.Insert(&person)
	if err == nil {
		u = person
	}
	return &u

}

func GetPersonById(id int) *Person {
	o := orm.NewOrm()
	person := Person{ID: id}
	o.Read(&person)
	return &person

}
//update

func UpdatePerson(id int, person Person) *Person {

	o := orm.NewOrm()
	u := Person{ID: id}
	//o.QueryTable("person").Filter("ID", id).Update(orm.Params{
		//"FirstName": person.FirstName,
		//"LastName": person.LastName,
	//})
	u.FirstName = person.FirstName
	u.LastName = person.LastName
	o.Update(&u)
	var p Person
	p = u
	return &p

}


		

