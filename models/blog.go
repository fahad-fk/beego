package models

import(
	"github.com/astaxie/beego/orm"
)

type Blog struct {
	//Parsing JSON into the struct
	ID        int   `json:"id"`
	Title string `json:"title"`
	Content  string `json:"content"`
}

func init() {
    // Need to register model in init
    orm.RegisterModel(new(Blog))
}


func InsertOneBlog(blog Blog) *Blog {
	o := orm.NewOrm()
	var u Blog
	_, err := o.Insert(&blog)
	if err == nil {
		u = blog
	}
	return &u

}


